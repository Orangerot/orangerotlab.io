---
title: "Tools to use for university"
date: 2022-07-03T23:24:01+02:00
slug: 2022-07-03-uni-tools
type: posts
categories:
  - default
tags:
  - default
---

University is hard so it is advised to make learning and working on exercises as
easy to begin and work with as possible. Efficiency is key for a procrastinator
like me. In this post I'll show you my tools and workflow for surviving in
university as an procrastinator. 

```
PFERD -> demenu / bemenu / pdefgrep -> zathura / xournalpp -> pen and paper
```
> My workflow as flow chart

## PFERD

PFERD is a tool that is specific to my university but I am sure that there is
such a tool for any institution. PFERD allows me to download all files of any of
my courses locally to my machine. That way I can work anywhere, work on some
exercises or rewatch some lectures without having to wait if I would have the
web UI. 

Of cause having a special tool for that is kinda stupid but unfortunately my
university does not provide an open standard for viewing and downloading the
contents of the courses. Such standards would be WebDAV, SFTP, FTPS, SSH, RSYNC
and so on. This is really unfortunate because even my highschool supported such
protocols. 

## demenu / bemenu

After I downloaded all of my courses I need a way to open the file I want as
fast as possible. Normally you would open your file explorer and click through
all your directories till you found the file you needed. Or you cd into
directories, which would do the same thing. This has some major disadvantages:
graphical applications are slow to begin with and since you don't know the
directory where the file is, searching through each directory is also
inefficient. 

A better why would be to search for a file. Searching for an exercise with a
number would also give you the exercises for other courses so would would need
to search in the directory of a course. But we could use the fact the we would
get result from every course if we search for all files and search through them
in a menu application like dmenu or bemenu. 

This way you can open the menu with a hotkey and start typing "[course] exercise
4"; enter and voila: you have your file open in like four seconds. 

The standard unix find is a bit slow (takes 20 seconds to print all files in my
home directory) to I searched long for a solution for that. Maybe I could patch
dmenu/bemenu to directly show an entry when the line came from stdin rather than
when all line have bin read. At the end I found a faster alternative to find
which is called fd. 

```
fd | bemenu -i -l10
```

## pdfgrep

When you work on an exercise and come accros a word you don't know and your prof
was not motivated enough as to write a script so you need to search through all
the slides of all lectures: use pdfgrep. 

It really felt like a tool from heaven after I wasted multiple hours opening
every slide and searching each of them every time. 

## zathura

It's just a great minimal and fast pdf reader with search function, vim-keybinding, marks with
quick jumps and a dark mode. 

## xournalpp

More bloated pdf annotation application. But perfect for highlighting the
important parts of a text exercise when drawing UML-Diagrams. 

## pen and paper

Better than any tablet. I might do a blog post specifically about this. 

## wolfram alpha

Must have for math and boolean expressions. 

## anki

Never used it. But have heard great things about it when you need to learn
things. It is much work though to set up the cards and everything. 
