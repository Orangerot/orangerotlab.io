---
title: "Project Ideas"
date: 2022-12-22T03:30:55+01:00
slug: 2022-12-22-project-ideas
type: posts
categories:
  - default
tags:
  - default
---

## All-in-One Keyboard PC

The ideal computer would just need two cables: one power cable from the wall to
the monitor; one usb c cable from the monitor to the keyboard (transfering the
image over Display Port and power for the Pi). 

Hardware
- 40% ortholinear keyboard with TrackPoint
- Planck Keyboard
- [Contra 40%](https://github.com/ai03-2725/Contra)
- (two) PSP / N3DS Joystick (PS Vita, ...) next to the space bar
- Pi 2040 / Pro Micro 
- CM4 / Pi 4
- Nvme
- USB C

Software:
- QMK
- FluidSynth
- https://ergogen.cache.works/
- KiCad

## Cheap and simple E-Paper tablet 

E-Paper tablets have the goal of replacing paper. The E-Paper display restricts
this devices functionality which helps keeping focused. But manifacturers then
build in _modern_ features like running a full android operating system,
connecting to the internet for downloading ebooks or syncing with their own
paid cloud, which in some cases is the recommended way to get your files to and
from the device! 

All it needs is a microcontroller and an SD-Card. The microcontroller needs to
be able to read the files from the SD-Card and render the pdf(?) on the
e-ink-display. Then it needs to read the pen position and draw it on to the pdf. 

Hardwarre:
- https://community.particle.io/t/e-ink-e-paper-display-with-touch-screen/48297
- Waveshare
- [Waveshare 7.5", 3 color e-paper display](https://www.waveshare.com/7.5inch-e-Paper-B.htm)
- [Waveshare e-paper SPI driver hat](https://www.waveshare.com/e-Paper-Driver-HAT.htm)
- [7" resistive touchscreen overlay](https://www.adafruit.com/product/1676)
- https://www.pollin.de/p/7-kapazitiver-touchscreen-kit-mit-usb-121709
- [SPI/I2C controller](https://www.adafruit.com/product/1571)
- Pi 2040

Software:
- pdf-render-lib: Poppler (C++), MuPDF (C), ...
- pdf-draw-lib: Cairo (C)
- Arduino/Pi bare metal: Low Levele Learning

